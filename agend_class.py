from contact import Contact 


class agenda:
    #A class that keeps a list of contact objects


    def __init__(self):
        self.contact_list=[]
        self.special_contacts={'Urgenta':112,'Serviciu Client':433,'Credit':333 }


    def add_contact(self,name,number):
        self.contact_list.append(Contact(name,number))


    def add_special_contact(self,name,number):
        self.special_contacts[name]=number


    def listing(self):
        i=3
        for key,item in self.special_contacts.items(): #special numbers
            print(key,': ',item)

        for item in self.contact_list:                  #rest of the agenda
            if i%5==0:
                print('\n')
            i=i+1
            item.display()

     
    def edit_contact(self): 
        option=Contact.menu.edit_contact_menu()

        if option=='1':                                                      #modify name
            old_name=input('Enter old contact name> ')
            new_name=input('Enter new contact name> ')
            for item in self.contact_list:
                if item.name==old_name:
                    item.modify_name(new_name)
                    return
            print('The contact you want to modify does not exist!')

        if option=='2':            
            name = input('Enter the contact name> ')                                         # modify number          
            old_number=int(input('Enter old number> '))
            new_number=int(input('Enter new number> '))
            for item in self.contact_list:
                if item.name==name:
                    item.modify_number(old_number,new_number)
                    return 
            print('The contact you want to modify does not exist!')

        if option=='3':            
            name = input('Enter the contact name> ')                                         # add number          
            new_number=int(input('Enter the number you want to add> '))
            for item in self.contact_list:
                if item.name==name:
                    item.add_number(new_number)
                    return
            print('The contact you want to modify does not exist!')

        if option=='4':            
            name = input('Enter the contact name> ')                                         # delete number          
            old_number=int(input('Enter the number you want to delete> '))
            for item in self.contact_list:
                if item.name==name:
                    item.delete_number(old_number)
                    return
            print('The contact you want to modify does not exist!')
                    

        

    def search_contact(self,cname):
        for item in self.contact_list:
            if cname == item.name:
                return cname
        return 'The contact you are looking for does not exist'


    def delete_contact(self,cname):
        for item in self.contact_list:
            if cname == item.name:
                self.contact_list.remove(item)



if __name__=='__main__':
    print(Contact.menu.main)
    a=agenda()
    a.add_contact('ilie',654)

    #print(a.search_contact('Other contact'))
    #print(a.search_contact('Credit'))
    a.add_contact('victor',6843520)
    a.add_contact('rady',63268356)
    #a.delete_contact('ilie')
    a.listing()
    a.edit_contact()
    a.listing()
