def func_30(names,years):
    age=[]
    if len(names)!=len(years):
        print('The lists does not have the same lenght')
        return

    for i in range(len(years)):
        age.append((names[i],years[i]))
    age=sorted(age,key=lambda tup:(tup[1]))
    return dict(age)

def func_31(names,years):
    age=[]
    if len(names)!=len(years):
        print('The lists does not have the same lenght')
        return

    for i in range(len(years)):
        age.append((names[i],years[i]))
    age=sorted(age,key=lambda tup:(tup[1]))
    res={}
    for (name,age) in age:
        res[name]=age
    return res




print('build-in list to dictionary',func_30(['gabi','roxana','andrei'],[1996,1997,1995]))

print('personal list to dictionary',func_31(['gabi','roxana','andrei'],[1996,1997,1995]))


