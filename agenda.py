agend={'anton':[12651328,65946132],'vics':[4565317653,3329681],'baba':[65312]}


def show(my_list):
    for name,number in my_list.items():
        print(name,': ',number)


def add_num_to_contact(my_list,name,number):
    if name in my_list:
        my_list[name].append(int(number))
    else:
        print ('*****The contact does not exist*****')


def add_contact(my_list,name,number=[]):
    if name in my_list.keys():
        my_list[name].append(number)
        return
    my_list[name]=number


def remove(my_list,name):
    my_list.pop(name)



def find_contact(my_list,name):
     print (name,': ',my_list[name])


while True:
    print('*'*15,' MENU ','*'*15)
    print('1 - Show agenda')
    print('2 - Add contact')
    print('3 - Add phone nr. to contact')
    print('4 - Find contact')
    print('5 - Delete contact')
    print('0 - Exit')
    print('*'*38)
    
    ch=input('Your option: ')
    while ch<'0' or ch>'5':
        ch=input('Wrong choise,choose again an option: ')

    if ch=='0':
        break
    elif ch=='1':
        show(agend)
    elif ch=='2':
        nume=input('Enter the name: ')
        numar=input('Enter the number: ')
        add_contact(agend,nume,numar)
    elif ch=='3':
        nume=input('Enter the name: ')
        numar=input('Enter the number: ')
        add_num_to_contact(agend,nume,numar)
    elif ch=='4':
        nume=input('Enter the name: ')
        find_contact(agend,nume)
    elif ch=='5':
        nume=input('Enter the name: ')
        remove(agend,nume)




