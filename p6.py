def func_6(input):
    if type(input) == str:
        return lambda x,y: -1 if len(x)<len(y) else (0 if len(x)==len(y) else 1)
    else:
        return lambda x,y: -1 if x<y else (0 if x==y else 1)

cmp1=func_6('str')
print(cmp1('ana are','ion ares'))

cmp2=func_6(6)
print(cmp2(3,2))