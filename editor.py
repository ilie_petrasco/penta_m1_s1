import sys


print(20 * '-')
print('File editor v 2.0')
print('-' * 20)


if len(sys.argv)==2:
    nume_fisier = sys.argv[1]
    print(nume_fisier)
else:
    nume_fisier=input('Numele fisierului> ')


line_number=1
handler=open(nume_fisier,'r')
for line in handler:
    print (line_number,line);
    line_number += 1
handler.close()


with open(nume_fisier, mode='a') as handler:
    while True:
        line=input(line_number)
        line_number += 1

        if line=='.':
            break

        handler.write(line+'\n')

