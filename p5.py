import math
def func_5(fn,iter):
    res=[]
    for i in iter:
        res.append(fn(i))
    return res

def root(a):
    return math.sqrt(a)

print(func_5(root,[9,16,225,169]))