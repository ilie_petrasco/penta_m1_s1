from menu import *	


class Contact:
	'''Class that represents a contact with name and number'''
	menu=Menu()


	def __init__(self,name,number):
		self.name = name
		self.number = {
						'Acasa': [],
						'Fix': [],
						'Mobil': []
						}
		self.add_number(number)

	def add_number(self,number):
		numer_type=self.menu.phone_type('add')
		if numer_type == '1':
			self.number['Acasa'].append(number)
		elif numer_type == '2':
			self.number['Fix'].append(number)
		elif numer_type == '3':
			self.number['Mobil'].append(number)



	def modify_name(self,name):
		self.name = name


	def modify_number(self,old_number,new_number):
		numer_type=self.menu.phone_type('modify')
		if numer_type == '1':
			if old_number in self.number['Acasa']: #if the old number exists then delete it 
				self.number['Acasa'].remove(old_number)
				self.number['Acasa'].append(new_number)
			else:
				print ('\''+self.name+'\''+' contact does not contains \'Acasa\' number you want to modify.')
		elif numer_type == '2':
			if old_number in self.number['Fix']: #if the old number exists then delete it 
				self.number['Fix'].remove(old_number)
				self.number['Fix'].append(new_number)
			else:
				print ('\''+self.name+'\''+' contact does not contains \'Fix\' number you want to modify.')
		elif numer_type == '3':
			if old_number in self.number['Mobil']: #if the old number exists then delete it 
				self.number['Mobil'].remove(old_number)
				self.number['Mobil'].append(new_number)
			else:
				print ('\''+self.name+'\''+' contact does not contains \'Mobil\' number you want to modify.')
		

	def delete_number(self,number):
		numer_type=self.menu.phone_type('delete')
		if numer_type == '1':
			if number in self.number['Acasa']:
				self.number['Acasa'].remove(number)
			else:
				print ('\''+self.name+'\''+' contact does not contains \'Acasa\' number you want to delete.')
		elif numer_type == '2':
			if number in self.number['Fix']:
				self.number['Fix'].remove(number)
			else:
				print ('\''+self.name+'\''+' contact does not contains \'Fix\' number you want to delete.')
		elif numer_type == '3':
			if number in self.number['Mobil']:
				self.number['Mobil'].remove(number)
			else:
				print ('\''+self.name+'\''+' contact does not contains \'Mobil\' number you want to delete.')


	def display(self):
		print (self.name,':')
		for key in self.number:
			if self.number[key]:                                                #display the list if is not empty
				print('\t',key,':',', '.join(str(x) for x in self.number[key]))

	def get_name(self):
		return self.name 	



if __name__=='__main__':
	c=Contact('ilie',4568)
	c.add_number(654)
	c.add_number(864)
	c.display()
	c.modify_number(654,111)
	c.display()
	c.delete_number(111)
	c.display()  