import sys


#vec=sys.argv[1:]
#vec=[int(i) for i in vec]


def func_10(*args):
    min=max=args[0]
    for argv in range(1,len(args)):
        if(args[argv]>max):
            max=args[argv]
        if(args[argv]<min):
            min=args[argv]
    return (min,max)

def func_11(*args):
    return (min(args),max(args))

print('personal: ',func_10(5,9,8,1,3,7,-7,6,66))
print('build-in: ',func_11(5,9,8,1,3,7,-7,6,66))