import sys

def func_4(file1,file2,file3):
    countries=open(file1,'r')
    visited=open(file2,'r')
    to_be_visited=open(file3,'w')

    vis=''
    for line in visited:
        vis+=line
    vis=vis.rstrip()

    for line in countries:
        if line not in vis:
            to_be_visited.write(line)

func_4(sys.argv[1],sys.argv[2],sys.argv[3])